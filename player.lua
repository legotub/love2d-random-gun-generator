lg = love.graphics


function player_load() 
	player = {}
	player.x = 640 / scale
	player.y = 5 / scale
	player.xvel = 0
	player.yvel = 0
	player.friction = 4.5
	player.speed = 3000
	player.width = 74 * scale
	player.height = 106 * scale
	player.onGround = false
	player.parkour = false
  playerLeft = lg.newImage("img/player/left.png")
  playerRight = lg.newImage("img/player/right.png")
  playerStill = lg.newImage("img/player/still.png")
	gravity = 900
	--Temporary Stuff
	groundlevel = love.window.getHeight() / scale
end


function player_physics(dt)
  groundlevel = love.window.getHeight() / scale
	player.x = player.x + player.xvel * dt
	player.y = player.y + player.yvel * dt
	if player.y + player.height >= groundlevel then
		player.onGround = true
	end
	if player.y + player.height < groundlevel then
		player.onGround = false
	end
	player.yvel = player.yvel + gravity * dt
	player.xvel = player.xvel * (1 - math.min(dt*player.friction, 1))
	if player.onGround == false then
		player.speed = 1500
	end
end

function player_move(dt)
	if love.keyboard.isDown("d") and
	player.xvel < player.speed then
		player.xvel = player.xvel + player.speed * dt
		end
	if love.keyboard.isDown("a") and
	player.xvel > -player.speed then
	player.xvel = player.xvel - player.speed * dt
	end
	if love.keyboard.isDown("w") and
	player.onGround == true then
		player.yvel = -500
	end
	if love.keyboard.isDown("lshift") and
	player.onGround == true then
		player.speed = 4500
	else
		player.speed = 2500
	end
end		

function player_boundary()
	if player.x < 0 then	
		player.x = 0
		player.xvel = 0
	end
	if player.x + player.width > 1280 then	
		player.x = 1280 - player.width
		player.xvel = 0
	end
	if player.y + player.height > groundlevel then
		player.y = groundlevel - player.height
		player.yvel = 0
	end
end


function player_draw()
		if player.xvel > 0 then
    lg.draw(playerRight,player.x,player.y)
		end
		if player.xvel < 0 then
    lg.draw(playerLeft,player.x,player.y)
		end
		if player.xvel == 0 then
		lg.draw(playerStill,player.x,player.y)
		end
end


function updatePlayer(dt)
	player_physics(dt)
	player_move(dt)
	player_boundary()
end



