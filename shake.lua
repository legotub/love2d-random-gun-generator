ScreenShake = {}

local intensity = 0
local limit = 10

-- add to intensity
function ScreenShake:trigger(force)
	intensity = intensity + force
	if intensity > limit then
		intensity = limit
	end
end

-- subtract from intensity
function ScreenShake:update(dt)
	if intensity > 0 then
		intensity = intensity - 20*dt
	end
end

-- draw the shake with random, intensity-based offsets
function ScreenShake:draw()
	local intensity = intensity
	if intensity > 0 then
		love.graphics.translate(
			(math.random() * 2 - 1) * intensity,
			(math.random() * 2 - 1) * intensity)
	end
end

-- this isn't really rocket science
