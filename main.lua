require "shake"
require "combinations"


function love.load()
gamestate = "menu"
lg = love.graphics
lg.setDefaultFilter("nearest")

mainx = 20
mainy = 20
scale = 1
shake = false


require "player"
player_load() 

--scopes
sixx = lg.newImage("img/scope/6x.png")
twox = lg.newImage("img/scope/2x.png")
onex = lg.newImage("img/scope/1x.png")
--main
ar = lg.newImage("img/main/AR.png")
silverAR = lg.newImage("img/main/silverAR.png")
laserAR = lg.newImage("img/main/laserAR.png")
--mags
mag1 = lg.newImage("img/magazine/mag1.png")
mag2 = lg.newImage("img/magazine/mag2.png")
mag3 = lg.newImage("img/magazine/mag3.png")
mag4 = lg.newImage("img/magazine/mag4.png")
--barrel
carbonfiberbarrel = lg.newImage("img/barrel/carbonfiber.png")
toxicbarrel = lg.newImage("img/barrel/toxic.png")
icebarrel = lg.newImage("img/barrel/ice.png")
laserbarrel = lg.newImage("img/barrel/laser.png")
orangebarrel = lg.newImage("img/barrel/orange.png")
--stock
woodenstock = lg.newImage("img/stock/wooden.png")
carbonfiberstock = lg.newImage("img/stock/carbonfiber.png")
toxicstock = lg.newImage("img/stock/toxic.png")
shockstock = lg.newImage("img/stock/shock.png")
metalstock = lg.newImage("img/stock/metal.png")
--grip
grip1 = lg.newImage("img/grip/grip1.png")


font = love.graphics.newFont(20)
lg.setFont(font)

bullets = { }


function randomize()

math.randomseed(os.time())

r = math.random(0,255)
g = math.random(0,255)
b = math.random(0,255)

scopechance = math.random(0,10)
  if scopechance >= 6 and scopechance <= 9 then
    scope = "6x"
  elseif scopechance >= 3 and scopechance <= 6 then
    scope = "2x"
  elseif scopechance >= 0 and scopechance <= 3 then
    scope = "1x"
  end
  
magchance = math.random(0,10)
    if magchance >= 7.5 and magchance <= 10 then
      mag = "mag1"
    elseif magchance >= 5 and magchance <= 7.5 then
      mag = "mag2"
    elseif magchance >= 2.5 and magchance <= 5 then
      mag = "mag3"
    elseif magchance >= 0 and magchance <= 2.5 then
      mag = "mag4"
    end
    
  stockchance = math.random(0,10)
    if stockchance >= 8 and stockchance <= 10 then
      stock = "woodenstock"
    elseif stockchance >= 6 and stockchance <= 8 then
      stock = "metal"
    elseif stockchance >= 4 and stockchance <= 6 then
      stock = "toxic"
    elseif stockchance >= 2 and stockchance <= 4 then
      stock = "shock"
    elseif stockchance >= 0 and stockchance <= 2 then
      stock = "carbonfiber"
    end  
    
    barrelchance = math.random(0,10)
    if barrelchance >= 8 and barrelchance <= 10 then
      barrel = "orange"
    elseif barrelchance >= 6 and barrelchance <= 8 then
      barrel = "laser"
    elseif barrelchance >= 4 and barrelchance <= 6 then
      barrel = "toxic"
    elseif barrelchance >= 2 and barrelchance <= 4 then
      barrel = "ice"
    elseif barrelchance >= 0 and barrelchance <= 2 then
      barrel = "carbonfiber"
    end 
    
    mainchance = math.random(0,10)
    if mainchance >= 6 and mainchance <= 9 then 
      main = "AR"
    elseif mainchance >= 3 and mainchance <= 6 then
      main = "silverAR"
    elseif mainchance >=0 and mainchance <=6 then
      main = "laserAR"
    end
      
      
 
    
  end  
 randomize()
 end


function love.draw()
  if gamestate == "playing" then
	ScreenShake:draw()
  lg.print("Possible combinations: ",1,1)
  lg.print("press enter to randomize",400,1)
  lg.print(combinations,250,1)
  lg.push()
    
	love.graphics.setColor(0,0,0,254)
	local i, o
	for i, o in ipairs(bullets) do
		love.graphics.rectangle('fill', o.x, o.y, 7, 5)
	end
  love.graphics.setColor(255, 255, 255, 224)
	lg.scale(scale)
	lg.setBackgroundColor(200,200,200)
  
  player_draw()
  
  if main == "AR" then
    lg.draw(ar,mainx,mainy)
  elseif main == "silverAR" then
    lg.draw(silverAR,mainx,mainy)
  elseif main == "laserAR" then
    lg.draw(laserAR,mainx,mainy)
  end
  
  if scope == "2x" then 
    lg.draw(twox,mainx +(ar:getWidth() / 1.3),mainy - onex:getHeight())
   elseif scope == "1x" then
    lg.draw(onex,mainx +(ar:getWidth() / 1.3),mainy - twox:getHeight())
   elseif scope == "6x" then
    lg.draw(sixx,mainx +(ar:getWidth() - 15),mainy - sixx:getHeight())
  end
  
  if mag == "mag1" then
    lg.draw(mag1,mainx + (ar:getWidth() / 2),mainy + ar:getHeight())
  elseif mag == "mag2" then
    lg.draw(mag2,mainx + (ar:getWidth() / 2),mainy + ar:getHeight())
  elseif mag == "mag3" then
    lg.draw(mag3,mainx + (ar:getWidth() / 2),mainy + ar:getHeight())
  elseif mag == "mag4" then
    lg.draw(mag4,mainx + (ar:getWidth() / 2),mainy + ar:getHeight())
  end
  
  if stock == "woodenstock" then
    lg.draw(woodenstock,mainx - woodenstock:getWidth(),mainy + (woodenstock:getHeight() / 3))
  elseif stock == "metal" then
    lg.draw(metalstock,mainx - metalstock:getWidth(),mainy + (metalstock:getHeight() / 3))
  elseif stock == "toxic" then
    lg.draw(toxicstock,mainx - toxicstock:getWidth(),mainy + (toxicstock:getHeight() / 3))
  elseif stock == "shock" then
    lg.draw(shockstock,mainx - shockstock:getWidth(),mainy + (shockstock:getHeight() / 3))
  elseif stock == "carbonfiber" then
    lg.draw(carbonfiberstock,mainx - carbonfiberstock:getWidth(),mainy + (carbonfiberstock:getHeight() / 3))
  end
  
  if barrel == "carbonfiber" then
	lg.draw(carbonfiberbarrel,mainx + ar:getWidth(),mainy + (ar:getHeight() / 2)-2)
  elseif barrel == "toxic" then
	lg.draw(toxicbarrel,mainx + ar:getWidth(),mainy + (ar:getHeight() / 2)-2)
  elseif barrel == "ice" then
	lg.draw(icebarrel,mainx + ar:getWidth(),mainy + (ar:getHeight() / 2)-2)
  elseif barrel == "laser" then
	lg.draw(orangebarrel,mainx + ar:getWidth(),mainy + (ar:getHeight() / 2)-2)
  elseif barrel == "orange" then
	lg.draw(laserbarrel,mainx + ar:getWidth(),mainy + (ar:getHeight() / 2)-2)
  end
  lg.setColor(r,g,b)
	lg.draw(grip1,mainx + (ar:getWidth() / 8), mainy + ar:getHeight())
  lg.setColor(255,255,255,254)
  lg.pop()
 end
 end




function love.update(dt)
updatePlayer(dt)
ScreenShake:update(dt)
random = math.random(0,10)
if love.mouse.isDown('l') then
	ScreenShake:trigger(.4)
	local direction = math.random(-1,1) / 10
    if random > 6 then
    table.insert(bullets, {
      x = barrel1x,
      y = barrel1y,
      dir = direction,
      speed = 1000
     
   })
  end
end

local i, o
for i, o in ipairs(bullets) do
	o.x = o.x + math.cos(o.dir) * o.speed * dt
	o.y = o.y + math.sin(o.dir) * o.speed * dt
	if (o.x < -10) or (o.x > love.graphics.getWidth() + 10)
	or (o.y < -10) or (o.y > love.graphics.getHeight() + 10) then
		table.remove(bullets, i)
	end
end

barrel1x = (mainx + ar:getWidth() + carbonfiberbarrel:getWidth()) * scale
barrel1y = (mainy + (ar:getHeight() / 2)-2 + (carbonfiberbarrel:getHeight() / 2))* scale

mainx = player.x + (player.width / 1.9) / scale
mainy = player.y + (player.height / 3) / scale


if gamestate == "playing" then
  if love.keyboard.isDown("return") then
    gamestate = "menu"
  end
elseif gamestate == "menu" then
  randomize()
  gamestate = "playing"
end
end




